# To-top app

## Setup

### 1. Install npm packages
```
npm run install
```

### 2. Serve the app
```
php artisan serve
```